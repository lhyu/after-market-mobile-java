package com.lhyu.aftermarket.mapper;

import com.lhyu.aftermarket.bean.TestBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TestMapper {
    TestBean getInfo(@Param("name") String name, @Param("password") String password);

    List<TestBean> test();
}
