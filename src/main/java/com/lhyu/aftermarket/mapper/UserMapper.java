package com.lhyu.aftermarket.mapper;

import com.lhyu.aftermarket.bean.UserBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {
    // 登录信息
    UserBean getInfo(@Param("user_id") String user_id,@Param("password") String password);

    List<UserBean> queryUser(@Param("user_id") String user_id,@Param("password") String password);
}
