package com.lhyu.aftermarket.mapper;

import com.lhyu.aftermarket.bean.CallbackImgListBean;
import com.lhyu.aftermarket.bean.CallbackListBean;
import com.lhyu.aftermarket.bean.QuestionBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BusinessMapper {
    List<QuestionBean> queryQuestion(@Param("keyword") String keyword);

    List<QuestionBean> queryDefaultQuestion();

    void insertCallBack(CallbackListBean callbackListBean);

    void insertCallBackImg(CallbackImgListBean callbackImgListBean);

    List<CallbackListBean> queryCallBack(CallbackListBean callbackListBean);

    List<CallbackImgListBean> queryCallBackImg(CallbackImgListBean callbackImgListBean);

    void updateCallBack(CallbackListBean callbackListBean);

    void updateCallBackImg(CallbackImgListBean callbackImgListBean);

    void deleteCallBackImg(CallbackImgListBean callbackImgListBean);
}
