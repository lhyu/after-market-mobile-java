package com.lhyu.aftermarket;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.lhyu.aftermarket.mapper")
public class AftermarketApplication {

    public static void main(String[] args) {
        SpringApplication.run(AftermarketApplication.class, args);
    }

}
