package com.lhyu.aftermarket.service;

import com.lhyu.aftermarket.bean.CallbackImgListBean;
import com.lhyu.aftermarket.bean.CallbackListBean;
import com.lhyu.aftermarket.bean.QuestionBean;

import java.util.List;

public interface BusinessService {
    List<QuestionBean> queryQuestion(String keyword);

    List<QuestionBean> queryDefaultQuestion();

    void insertCallBack( CallbackListBean callbackListBean);

    void insertCallBackImg(CallbackImgListBean callbackImgListBean);

    List<CallbackListBean> queryCallBack(CallbackListBean callbackListBean);

    List<CallbackImgListBean> queryCallBackImg(CallbackImgListBean callbackImgListBean);

    void updateCallBack( CallbackListBean callbackListBean);

    void updateCallBackImg(CallbackImgListBean callbackImgListBean);

    void deleteCallBackImg(CallbackImgListBean callbackImgListBean);
}
