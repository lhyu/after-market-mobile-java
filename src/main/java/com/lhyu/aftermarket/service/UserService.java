package com.lhyu.aftermarket.service;

import com.lhyu.aftermarket.bean.UserBean;

import java.util.List;

public interface UserService {
    UserBean loginIn(String user_id, String password);

    List<UserBean> queryUser(String user_id, String password);
}
