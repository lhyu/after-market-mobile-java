package com.lhyu.aftermarket.service;

import com.lhyu.aftermarket.bean.TestBean;

import java.util.List;

public interface TestService {
    TestBean testLoginIn(String name, String password);

    // 测试
    List<TestBean> test();
}
