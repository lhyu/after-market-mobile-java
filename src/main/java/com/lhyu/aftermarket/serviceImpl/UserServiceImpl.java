package com.lhyu.aftermarket.serviceImpl;

import com.lhyu.aftermarket.bean.UserBean;
import com.lhyu.aftermarket.mapper.UserMapper;
import com.lhyu.aftermarket.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {
    //将DAO注入Service层
    @Autowired
    private UserMapper userMapper;

    @Override
    public UserBean loginIn(String user_id, String password) {
        return userMapper.getInfo(user_id,password);
    }

    @Override
    public List<UserBean> queryUser(String user_id, String password) {
        return userMapper.queryUser(user_id,password);
    }

}
