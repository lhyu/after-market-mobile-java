package com.lhyu.aftermarket.serviceImpl;

import com.lhyu.aftermarket.bean.TestBean;
import com.lhyu.aftermarket.mapper.TestMapper;
import com.lhyu.aftermarket.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestServiceImpl  implements TestService {
    //将DAO注入Service层
    @Autowired
    private TestMapper testMapper;

    @Override
    public TestBean testLoginIn(String name, String password) {
        return testMapper.getInfo(name,password);
    }

    @Override
    public List<TestBean> test() {
        return testMapper.test();
    }
}
