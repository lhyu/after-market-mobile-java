package com.lhyu.aftermarket.serviceImpl;

import com.lhyu.aftermarket.bean.CallbackImgListBean;
import com.lhyu.aftermarket.bean.CallbackListBean;
import com.lhyu.aftermarket.bean.QuestionBean;
import com.lhyu.aftermarket.mapper.BusinessMapper;
import com.lhyu.aftermarket.service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusinessServiceImpl  implements BusinessService {
    //将DAO注入Service层
    @Autowired
    private BusinessMapper businessMapper;
    // 查询
    @Override
    public List<QuestionBean> queryQuestion(String keyword) {
        return businessMapper.queryQuestion(keyword);
    }

    @Override
    public List<QuestionBean> queryDefaultQuestion() {
        return businessMapper.queryDefaultQuestion();
    }
    // 插入
    @Override
    public void insertCallBack(CallbackListBean callbackListBean) {
        businessMapper.insertCallBack(callbackListBean);
    }

    // 插入
    @Override
    public void insertCallBackImg(CallbackImgListBean callbackImgListBean) {
        businessMapper.insertCallBackImg(callbackImgListBean);
    }

    // 查询
    @Override
    public List<CallbackListBean> queryCallBack(CallbackListBean callbackListBean) {
        return businessMapper.queryCallBack(callbackListBean);
    }

    // 查询
    @Override
    public List<CallbackImgListBean> queryCallBackImg(CallbackImgListBean callbackImgListBean) {
        return businessMapper.queryCallBackImg(callbackImgListBean);
    }

    // 修改
    @Override
    public void updateCallBack(CallbackListBean callbackListBean) {
        businessMapper.updateCallBack(callbackListBean);
    }

    // 修改
    @Override
    public void updateCallBackImg(CallbackImgListBean callbackImgListBean) {
        businessMapper.updateCallBackImg(callbackImgListBean);
    }

    // 删除
    @Override
    public void deleteCallBackImg(CallbackImgListBean callbackImgListBean) {
        businessMapper.deleteCallBackImg(callbackImgListBean);
    }
}
