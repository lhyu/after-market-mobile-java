package com.lhyu.aftermarket.utils;

import java.util.Random;
import java.util.UUID;

/**
 * 生成UUID
 *
 * @author lhy
 * @ClassName: Util
 * @Description: 为DB的主键生成唯一的UUID
 * @date 2021-7-30 上午13:30:00
 */
public class UUIDGenerator {

    public UUIDGenerator() {
    }


    public static String getUUID() {
        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        // 去掉"-"符号
        String temp = str.substring(0, 8) + str.substring(9, 13)
                + str.substring(14, 18) + str.substring(19, 23)
                + str.substring(24);
        return temp;
    }

    //length用户要求产生字符串的长度
    public static String getRandomString(int length){
        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random=new Random();
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<length;i++){
            int number=random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }
}

