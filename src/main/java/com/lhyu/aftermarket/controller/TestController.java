package com.lhyu.aftermarket.controller;

import com.lhyu.aftermarket.bean.Result;
import com.lhyu.aftermarket.bean.TestBean;
import com.lhyu.aftermarket.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class TestController {
    //将Service注入Web层
    @Autowired
    TestService testService;

    @RequestMapping("/index")
    public String sayHello(){
        return "index";
    }

    @RequestMapping("/test")
    public String show(){
        return "testLogin";
    }
    @RequestMapping(value = "/testLoginIn",method = RequestMethod.POST)
    public String testLoginIn(String name,String password){
        TestBean testBean = testService.testLoginIn(name,password);
        if(testBean!=null){
            return "success";
        }else {
            return "error";
        }
    }

    @RequestMapping("/api/test")
    @ResponseBody
    Map<String,Object> test(){
        Map<String,Object> returnMap = new HashMap<>();
        Result result = new Result();
        result.setCode("0");
        result.setMessage("success");
        try {
            List<TestBean> test=testService.test();
            returnMap.put("data",test);
        }catch (Exception e){
            e.printStackTrace();
            result.setCode("-1");
            result.setMessage(e.getMessage());
        }
        returnMap.put("result",result);
        return returnMap;
    }
}
