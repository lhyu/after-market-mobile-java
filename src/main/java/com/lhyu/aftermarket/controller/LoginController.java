package com.lhyu.aftermarket.controller;

import com.lhyu.aftermarket.bean.Result;
import com.lhyu.aftermarket.bean.UserBean;
import com.lhyu.aftermarket.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class LoginController {
    //将Service注入Web层
    @Autowired
    UserService userService;

    @RequestMapping(value="/login",method=RequestMethod.POST)
    Map<String,Object> index(@RequestBody UserBean userBean){
        Map<String,Object> returnMap = new HashMap<>();
        Result result = new Result();
        result.setCode("0");
        result.setMessage("成功");
        try {
            List<UserBean> list=userService.queryUser(userBean.getUser_id(), userBean.getPassword());
            returnMap.put("list",list.get(0));
        }catch (Exception e){
            e.printStackTrace();
            result.setCode("-1");
            result.setMessage("用户ID或密码输入错误");
        }
        returnMap.put("result",result);
        return returnMap;
    }

}
