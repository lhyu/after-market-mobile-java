package com.lhyu.aftermarket.controller;

import com.lhyu.aftermarket.bean.CallbackImgListBean;
import com.lhyu.aftermarket.bean.CallbackListBean;
import com.lhyu.aftermarket.bean.QuestionBean;
import com.lhyu.aftermarket.bean.Result;
import com.lhyu.aftermarket.service.BusinessService;
import com.lhyu.aftermarket.utils.UUIDGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class businessController {
    // 将Service注入Web层
    @Autowired
    BusinessService businessService;

    // 问题列表
    @RequestMapping(value="/queryQuestionList",method=RequestMethod.POST)
    Map<String,Object> index(@RequestBody QuestionBean questionBean){
        Map<String,Object> returnMap = new HashMap<>();
        Result result = new Result();
        result.setCode("0");
        result.setMessage("成功");
        try {
            List<QuestionBean> list=businessService.queryQuestion(questionBean.getKeyword());
            returnMap.put("list",list);
        }catch (Exception e){
            e.printStackTrace();
            result.setCode("-1");
            result.setMessage("列表获取错误");
        }
        returnMap.put("result",result);
        return returnMap;
    }

    // 默认问题
    @RequestMapping(value="/queryDefaultQuestionList",method=RequestMethod.POST)
    Map<String,Object> defaultQuestion(){
        Map<String,Object> returnMap = new HashMap<>();
        Result result = new Result();
        result.setCode("0");
        result.setMessage("成功");
        try {
            List<QuestionBean> list=businessService.queryDefaultQuestion();
            returnMap.put("list",list);
        }catch (Exception e){
            e.printStackTrace();
            result.setCode("-1");
            result.setMessage("列表获取错误");
        }
        returnMap.put("result",result);
        return returnMap;
    }

    // 插入反馈
    @RequestMapping(value="/insertCallBack",method=RequestMethod.POST)
    Map<String,Object> insertCallBack(@RequestBody CallbackListBean callbackListBean,CallbackImgListBean callbackImgListBean){
        Map<String,Object> returnMap = new HashMap<>();
        Result result = new Result();
        result.setCode("0");
        result.setMessage("成功");
//        CallbackImgListBean callbackImgListBean = new CallbackImgListBean();
//        System.out.println(callbackListBean.getCallbackImgList());
//        System.out.println(callbackListBean.getCallbackImgList().size());
        String eventId = UUIDGenerator.getRandomString(12);
        try {
            for(int i=0;i<callbackListBean.getCallbackImgList().size(); i++){
                callbackImgListBean.setImgName(callbackListBean.getCallbackImgList().get(i).getImgName());
                callbackImgListBean.setImgContent(callbackListBean.getCallbackImgList().get(i).getImgContent());
                callbackImgListBean.setEventId(eventId);
                callbackImgListBean.setId(UUIDGenerator.getUUID());
                callbackImgListBean.setUserId(callbackListBean.getUserId());
                businessService.insertCallBackImg(callbackImgListBean);
            }
            callbackListBean.setId(UUIDGenerator.getUUID());
            callbackListBean.setType(callbackListBean.getType());
            callbackListBean.setDescribes(callbackListBean.getDescribes());
            callbackListBean.setUserId(callbackListBean.getUserId());
            callbackListBean.setEventId(eventId);
            callbackListBean.setPhone(callbackListBean.getPhone());
            callbackListBean.setDate(callbackListBean.getDate());
            callbackListBean.setNowDate(new Date());
            businessService.insertCallBack(callbackListBean);
        }catch (Exception e){
            e.printStackTrace();
            result.setCode("-1");
            result.setMessage("列表获取错误");
        }
        returnMap.put("result",result);
        return returnMap;
    }

    // 反馈列表
    @RequestMapping(value="/queryCallBack",method=RequestMethod.POST)
    Map<String,Object> queryCallBack(@RequestBody CallbackListBean callbackListBean){
        Map<String,Object> returnMap = new HashMap<>();
        Result result = new Result();
        result.setCode("0");
        result.setMessage("成功");
        try {
            List<CallbackListBean> list=businessService.queryCallBack(callbackListBean);
            returnMap.put("list",list);
        }catch (Exception e){
            e.printStackTrace();
            result.setCode("-1");
            result.setMessage("列表获取错误");
        }
        returnMap.put("result",result);
        return returnMap;
    }

    // 反馈图片列表
    @RequestMapping(value="/queryCallBackImg",method=RequestMethod.POST)
    Map<String,Object> queryCallBackImg(@RequestBody CallbackImgListBean callbackImgListBean){
        Map<String,Object> returnMap = new HashMap<>();
        Result result = new Result();
        result.setCode("0");
        result.setMessage("成功");
        try {
            List<CallbackImgListBean> list=businessService.queryCallBackImg(callbackImgListBean);
            returnMap.put("list",list);
        }catch (Exception e){
            e.printStackTrace();
            result.setCode("-1");
            result.setMessage("列表获取错误");
        }
        returnMap.put("result",result);
        return returnMap;
    }

    // 修改反馈
    @RequestMapping(value="/updateCallBack",method=RequestMethod.POST)
    Map<String,Object> updateCallBack(@RequestBody CallbackListBean callbackListBean,CallbackImgListBean callbackImgListBean){
        Map<String,Object> returnMap = new HashMap<>();
        Result result = new Result();
        result.setCode("0");
        result.setMessage("成功");
        //String eventId = UUIDGenerator.getRandomString(12);
        try {
            callbackImgListBean.setUserId(callbackListBean.getUserId());
            callbackImgListBean.setEventId(callbackListBean.getEventId());
            businessService.deleteCallBackImg(callbackImgListBean);
            for(int i=0;i<callbackListBean.getCallbackImgList().size(); i++){
                callbackImgListBean.setImgName(callbackListBean.getCallbackImgList().get(i).getImgName());
                callbackImgListBean.setImgContent(callbackListBean.getCallbackImgList().get(i).getImgContent());
                callbackImgListBean.setEventId(callbackListBean.getEventId());
                callbackImgListBean.setId(UUIDGenerator.getUUID());
                callbackImgListBean.setUserId(callbackListBean.getUserId());
                businessService.insertCallBackImg(callbackImgListBean);
            }
            callbackListBean.setId(UUIDGenerator.getUUID());
            callbackListBean.setType(callbackListBean.getType());
            callbackListBean.setDescribes(callbackListBean.getDescribes());
            callbackListBean.setUserId(callbackListBean.getUserId());
            callbackListBean.setEventId(callbackListBean.getEventId());
            callbackListBean.setPhone(callbackListBean.getPhone());
            callbackListBean.setDate(callbackListBean.getDate());
            callbackListBean.setNowDate(new Date());
            businessService.updateCallBack(callbackListBean);
        }catch (Exception e){
            e.printStackTrace();
            result.setCode("-1");
            result.setMessage("列表获取错误");
        }
        returnMap.put("result",result);
        return returnMap;
    }
}
