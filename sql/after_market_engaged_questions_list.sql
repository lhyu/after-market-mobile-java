/*
 Navicat Premium Data Transfer

 Source Server         : 本地mysql
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : aftermarket

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 30/07/2021 18:52:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for after_market_engaged_questions_list
-- ----------------------------
DROP TABLE IF EXISTS `after_market_engaged_questions_list`;
CREATE TABLE `after_market_engaged_questions_list`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `answer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of after_market_engaged_questions_list
-- ----------------------------
INSERT INTO `after_market_engaged_questions_list` VALUES ('1', '张三', '第三得分点', 'default');
INSERT INTO `after_market_engaged_questions_list` VALUES ('2', '唐三藏', '大师傅士大夫', NULL);
INSERT INTO `after_market_engaged_questions_list` VALUES ('3', '三角形', '上大师傅大师傅', NULL);
INSERT INTO `after_market_engaged_questions_list` VALUES ('4', '士大夫三', '但是犯得上房贷首付但是', NULL);
INSERT INTO `after_market_engaged_questions_list` VALUES ('5', '王五', '士大夫大师傅但是', NULL);

SET FOREIGN_KEY_CHECKS = 1;
