/*
 Navicat Premium Data Transfer

 Source Server         : 本地mysql
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : aftermarket

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 30/07/2021 18:51:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for after_market_callback_list
-- ----------------------------
DROP TABLE IF EXISTS `after_market_callback_list`;
CREATE TABLE `after_market_callback_list`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '提交建议类型: 反馈question/建议suggest',
  `describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '描述',
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '状态',
  `userId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户id',
  `eventId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '本次事件的Id',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系方式 电话/邮箱',
  `nowDate` datetime(6) NULL DEFAULT NULL COMMENT '提出反馈时间',
  `date` datetime(6) NULL DEFAULT NULL COMMENT '问题发生时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of after_market_callback_list
-- ----------------------------
INSERT INTO `after_market_callback_list` VALUES ('4e19093d2fb74d1fb557f15bd47462b4', 'question', '44', NULL, 'lhy-1', 'J77CYCzhkWwj', '44', '2021-07-30 16:44:46.640000', NULL);
INSERT INTO `after_market_callback_list` VALUES ('52ac87ec05bd4cc5821b221b38e50ddb', 'question', '1', NULL, 'lhy-1', 'mbnDlFZxoeeb', '2', '2021-07-30 16:42:22.610000', NULL);
INSERT INTO `after_market_callback_list` VALUES ('5e36f6f632314f01a47ef0b66f93e7ad', 'question', '1', NULL, 'lhy-1', 'AHQaoUqxkIEk', '2', '2021-07-30 16:26:10.298000', NULL);
INSERT INTO `after_market_callback_list` VALUES ('6719a08dad5d4df3b67f18a5e8909ad1', 'question', '44', NULL, 'lhy-1', 'ADu5gjiJ1fAL', '44', '2021-07-30 16:43:31.036000', NULL);
INSERT INTO `after_market_callback_list` VALUES ('7a45f891956c44f5ae6d1f1ad9bec0db', 'question', '1', NULL, 'lhy-1', 'OJMaTB9HEdQS', '2', '2021-07-30 16:32:17.746000', NULL);
INSERT INTO `after_market_callback_list` VALUES ('ab5f0a3599d54a50bb08a98320e0c8c7', 'question', '55', NULL, 'lhy-1', 'bfLk9nRRCLw6', '555', '2021-07-30 16:56:09.594000', NULL);
INSERT INTO `after_market_callback_list` VALUES ('cf03c46913d949a890c6805c271f68c7', 'question', '3', NULL, 'lhy-1', 'Yk4DH5XY4aJ6', '3', '2021-07-30 16:17:51.214000', NULL);
INSERT INTO `after_market_callback_list` VALUES ('f2dcc95354764b1f976dc5166660557a', 'question', '3', NULL, 'lhy-1', '8CKZkwg6DRhN', '3', '2021-07-30 16:18:46.970000', NULL);
INSERT INTO `after_market_callback_list` VALUES ('fb945e76a06947e4833c28ef9a8866bc', 'question', '55', NULL, 'lhy-1', '3lQYS0kWJzW6', '555', '2021-07-30 16:57:07.827000', NULL);
INSERT INTO `after_market_callback_list` VALUES ('fd4ebefc1db14777a3dc9468a2f413db', 'question', '44', NULL, 'lhy-1', 'vCTYPjGkJWhD', '44', '2021-07-30 16:55:22.537000', NULL);

SET FOREIGN_KEY_CHECKS = 1;
